***Settings***
Library    SeleniumLibrary

***Variables***
${URL}     https://opensource-demo.orangehrmlive.com/

***Keywords***
LoginPage
        Input Text         id=txtUsername    Admin
        Input Password     id=txtPassword    admin123
        Click Button       id=btnLogin


***Test Case***
SampleLoginTestCasewithkeywords
        [Documentation]    this is sample login test case
        Open Browser       ${URL}    chrome
        Set Browser Implicit Wait    5
        LoginPage
        Click Element      id=welcome 
        Click Element      link=Logout
        Close Browser
        Log    TestComplete          
        

